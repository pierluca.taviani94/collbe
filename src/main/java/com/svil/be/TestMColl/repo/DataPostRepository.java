package com.svil.be.TestMColl.repo;

import com.svil.be.TestMColl.domain.Post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DataPostRepository extends CrudRepository<Post, Long>, JpaSpecificationExecutor<Post> {

    @Query("SELECT t FROM Post t WHERE t.user_id = :user_id")
    Post getByUserId(@Param("user_id") Integer user_id);
}
