package com.svil.be.TestMColl.controller;

import com.svil.be.TestMColl.domain.Post;
import com.svil.be.TestMColl.service.DataPostService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/my-api")
public class DataPostController {

    private DataPostService dataPostService;

    public DataPostController(DataPostService dataPostService) {
        this.dataPostService = dataPostService;
    }

    @GetMapping("/posts")
    @CrossOrigin(origins = "http://localhost:8080")
    public Iterable<Post> list() {
        return dataPostService.list();
    }

    /*@GetMapping("/posts/{id}")
    public Post listPostOfId(@PathVariable long id) {
        return dataPostService.getPostById(id);
    }*/

    /*
    * need to add v2 for ambiguos error
    * */
    @GetMapping("/posts/v2")
    public Post listPostUserId(@RequestParam Integer user_id) {
        return dataPostService.getPostByUserId(user_id);
    }
}
