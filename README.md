# CollBE

You are asked to implement a simple REST API which follow these requirements:
GIVEN a GET call to /posts
WHEN no additional parameter is passed
THEN a list of posts taken by https://gorest.co.in/public/v1/posts is
returned

Eg: http://localhost:8081/my-api/posts

GIVEN a GET call to /posts with no additional parameter
WHEN the user_id Query Parameter is passed
THEN a list of products taken by https://gorest.co.in/public/v1/posts is

returned filtered by user_id

Eg: http://localhost:8081/my-api/posts?user_id=2

Note
• Use whatever technology you prefer, but as the position says Java/Kotlin
with SpringBoot is preferential
• mvn clean install or equivalent (groovy) should pass
• Don’t be afraid to ask questions if something is unclear
• Keep it simple
• You can deliver it as a public GitHub/GitLab project or zip it and attach to
an email
What we value most
• The project respects its requirements and it’s functional
• SOLID principles have been applied
• Unit Tests are well written
