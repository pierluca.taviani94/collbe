package com.svil.be.TestMColl.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataGoRestTest {

    @JsonProperty("meta")
    private JsonNode meta;

    @JsonProperty("data")
    private JsonNode data;

    public JsonNode getMeta() {
        return meta;
    }

    public void setMeta(JsonNode meta) {
        this.meta = meta;
    }

    public JsonNode getData() {
        return data;
    }

    public void setData(JsonNode data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "DataGoRestV2{" +
                "meta=" + meta +
                ", data=" + data +
                '}';
    }
}

