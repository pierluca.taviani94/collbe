package com.svil.be.TestMColl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.svil.be.TestMColl.domain.Post;
import com.svil.be.TestMColl.model.DataGoRestTest;
import com.svil.be.TestMColl.service.DataPostService;
import lombok.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/*
* TO DO :
*
* Get data from URL ad rest a list of post
*
* **/
@SpringBootApplication
public class TestMCollApplication {

	private String url = "https://gorest.co.in/public/v1/posts";

	public static void main(String[] args) {
		SpringApplication.run(TestMCollApplication.class, args);
	}

	private static final Logger log = LoggerFactory.getLogger(TestMCollApplication.class);

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {

		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/my-api").allowedOrigins("http://localhost:8080");
			}
		};

	}

	@Bean
	public CommandLineRunner run(DataPostService dataPostService, RestTemplate restTemplate) throws Exception {

		return args -> {
			log.info("MAIN START");

			ObjectMapper mapper = new ObjectMapper();
			TypeReference<List<Post>> typeReference = new TypeReference<List<Post>>() {};

			DataGoRestTest dataGoRest = restTemplate.getForObject(url, DataGoRestTest.class);
			//dataGoRest.getData();
			//log.info("DATA GET : {}", dataGoRest.toString());

			try {
				List<Post> posts = mapper.readValue(dataGoRest.getData().toString(), typeReference);

				//log.info("DATA : {}",dataGoRest.getData().toString());
				//log.info("POSTS : {}",posts.toString());

				dataPostService.save(posts);
				log.info("User Save");

			} catch (Exception e) {
				log.error(e.toString());
			}
		};
	}
}
