package com.svil.be.TestMColl.service;


import com.svil.be.TestMColl.domain.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.svil.be.TestMColl.repo.DataPostRepository;

import java.util.List;

@Service
public class DataPostService {

    @Autowired
    private DataPostRepository dataPostRepository;

    public DataPostService(DataPostRepository dataPostRepository) {
        this.dataPostRepository = dataPostRepository;
    }

    public Iterable<Post> list() {
        return dataPostRepository.findAll();
    }

    public Post save(Post post) {
        return dataPostRepository.save(post);
    }

    public void save(List<Post> posts) {
        dataPostRepository.saveAll(posts);
    }

    /*
    public Post getPostById(long id) {
        return dataPostRepository.findById(id).get();
    }*/

    public Post getPostByUserId(Integer userId) {
        return dataPostRepository.getByUserId(userId);
    }
}
