package com.svil.be.TestMColl.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
/*
 * DTO
 * **/
@Data
@Entity
public class Post {

    @Id
    private Integer id; //Long for findById
    private Integer user_id;
    private String title;

    //@Length(min = 3, max = 15)
    @Lob
    private String body;

    public Post() {}

    public Post(Integer id, Integer user_id, String title, String body) {
        this.id = id;
        this.user_id = user_id;
        this.title = title;
        this.body = body;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

